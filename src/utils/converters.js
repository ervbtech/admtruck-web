import numeral from 'numeral'
import moment  from 'moment'
import ptBr    from 'numeral/min/languages/pt-br.min'

export class NumberFormatValueConverter {
   toView(value, format, lang = 'pt-br') {
    numeral.language(lang, ptBr)
    numeral.language(lang)
    return numeral(value).format(format)
   }
};

export class DateFormatValueConverter {
    toView(value, format) {
        return moment(value).format(format)
    }

    fromView(value) {
        return moment(value)
    }
}
