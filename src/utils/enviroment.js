'use strict'

export class Enviroment {

  constructor() {
    this.host = window.location.hostname ==='localhost' ? 'http://localhost:3000/' : ''
    this.api = this.host + 'api/'
    this.requesting  = false;
  }

  getUri(path) {
    return this.api + path;
  }

}
