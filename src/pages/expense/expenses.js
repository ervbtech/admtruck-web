import {inject}           from 'aurelia-framework'
import {DialogService}    from 'aurelia-dialog'
import {ExpenseService}   from 'services/expense_service'
import {MessageService}   from 'services/message_service'
import {ConfirmModal}     from 'components/confirm-modal'
import {BasePage}         from 'pages/base_page'
import {Enviroment}         from 'utils/enviroment'

let service = {}, ctrl = {}
let dialogService = {}, messageService = {}

@inject(ExpenseService, DialogService, MessageService, Enviroment)
export class Expenses extends BasePage {

  constructor(ExpenseService, DialogService, MessageService, Enviroment) {

    super(MessageService, Enviroment)

    service = ExpenseService
    dialogService = DialogService
    messageService = MessageService

    ctrl = this

    this.expense = { loan: null, date: new Date() }
    this.errors = []

    this.types = [
      { value: 'loan', label: 'Empréstimo'}
      , { value: 'eventual', label: 'Eventual'}
      , { value: 'operational', label: 'Operacional'}
      , { value: 'taxable', label: 'Tributável'}
    ]

    service.findAll()
    .then(response => response.json())
    .then(data => {
      this.expenses = data
      this.loadCompleted();
    })

    service.findAllContracts()
    .then(response => response.json())
    .then(data => {
      this.contracts = []
      for (var i = 0; i < data.length; i++) {
        this.contracts.push({
          label: data[i].name + ' - ' + data[i].contractingCompany
          , value: data[i]._id
        })
      }
    })

    this.loadTrucks()

    service.findAllEmployees()
    .then(response => response.json())
    .then(data => {
      this.employees = []
      for (var i = 0; i < data.length; i++) {
        this.employees.push({
          label: data[i].name
          , value: data[i]._id
        })
      }
    })

    service.findAllLoans()
    .then(response => response.json())
    .then(data => {
      this.loans = []
      for (var i = 0; i < data.length; i++) {
        this.loans.push({
          label: data[i].contractNumber
          , value: data[i]._id
        })
      }
    })


  }

  loadTrucks(query) {
    service.findAllTrucks(query)
    .then(response => response.json())
    .then(data => {
      this.trucks = []
      for (var i = 0; i < data.length; i++) {
        this.trucks.push({
          label: data[i].plate
          , value: data[i]._id
        })
      }
    })
  }

  fireChangeContract(event) {
    this.expense.contract = event.detail.value
    this.loadTrucks(this.expense.contract)
  }

  fireChangeDate(event) {
    this.expense.date = event.detail.value
  }

  fireChangeLoan(event) {
    this.expense.loan = event.detail.value
  }

  fireChangeTruck(event) {
    this.expense.truck = event.detail.value
  }

  fireChangeEmployee(event) {
    this.expense.employee = event.detail.value
  }

  fireChangeType(event) {
    this.expense.type = event.detail.value
  }

  fireChangeValue(event) {
    this.expense.value = event.detail.value
  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.expense = data
        this.refreshCustomComponents()
      })
    }
  }

  save() {
    service.save(this.expense)
    .then(response => response.json())
    .then(data => {
      service.findAll()
      .then(response => response.json())
      .then(data => {
        messageService.showSuccess('Operação realizada com sucesso.')
        this.clear()
        this.expenses = data
      })
    })
    .catch((err) => {
      messageService.showError('Ops... ocorreu um erro durante a operação.')
    })
  }

  clear() {
    this.expense = {}
    window.location.href = '#/expenses'
    $('select2').find('select:first').val(null).trigger('change')
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          messageService.showSuccess('Registro removido com sucesso.')
        })
      }
    });
  }

  getTypeName(id) {
    for(let index in this.types) {
      let type = this.types[index]
      if(type.value === id) return type.label
    }
    return ''
  }

}
