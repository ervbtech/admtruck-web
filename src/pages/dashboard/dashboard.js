import {inject}           from 'aurelia-framework'
import {IncomeChart}      from '../dashboard/charts/income_chart'
import {ProductionChart}  from '../dashboard/charts/production_chart'
import {DashboardService} from 'services/dashboard_service'
import moment             from 'moment'

import 'chart.js'
import 'malihu-custom-scrollbar-plugin'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css!'
import 'bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR'

let service = {}, incomeChart = {}, productionChart = {}

let backgroundColor = [
  "#455C73",
  "#9B59B6",
  "#BDC3C7",
  "#26B99A",
  "#3498DB"
], hoverBackgroundColor = [
  "#34495E",
  "#B370CF",
  "#CFD4D8",
  "#36CAAB",
  "#49A9EA"
]

@inject(IncomeChart, ProductionChart, DashboardService, Element)
export class Dashboard {

  constructor(IncomeChart, ProductionChart, DashboardService, element) {
    this.element = element
    service = DashboardService
    incomeChart = IncomeChart
    productionChart = ProductionChart

    /** TOTAL FIELDS */
    this.headerTotals = {
      totalEarnings: 0.0,
      totalExpense: 0.0,
      profit: 0.0,
      totalCash: 0.0,
      percentEarningsLast: 0.0,
      percentExpenseLast: 0.0,
      percentProfitLast: 0.0,
      percentTotalCashLast: 0.0
    };

  }

  attached() {
    this.configCharts(moment())
    // Tooltip
    $('[data-toggle="tooltip"]').tooltip({
      container: 'body'
    });

    $('.input-group.date').datepicker({
        format: {
                toDisplay: function (date, format, language) {
                    return moment(date).add(1, 'day').format('MMMM [de] YYYY')
                },
                toValue: function (date, format, language) {
                    return date;
                }
        },
        minViewMode: 1,
        language: "pt-BR",
        autoclose: true,
        templates: {
              leftArrow: '<i class="fa fa-arrow-left"></i>',
              rightArrow: '<i class="fa fa-arrow-right"></i>',
        }
    }).on('changeDate', (evt) => {
      this.changeFilter(evt);
    });

    $('.input-group.date').datepicker('setDate', moment().toDate());

  }

  configCharts($param) {
    this.filter = $param

    let filter = $param.format('YYYYMMDD')
    this.incomeChart =  incomeChart.config()
    this.productionChart = productionChart.config()

    this.contributionTruckInfo = []
    this.truckExpensesInfo = []

    service.getContractRevenuesDataChart(filter)
    .then(response => response.json())
    .then((data) => {
      this.contractRevenues = data
    })

    service.getContractExpensesDataChart(filter)
    .then(response => response.json())
    .then((data) => {
      this.contractCosts = []
      for(var i = 0; i < data.length; i++) {
        let contract = data[i]
        contract.fragment = (contract.progress / 10 + 1) > 10 ? 10 : parseInt((contract.progress / 10 + 1))
        this.contractCosts.push(contract);
      }
    })

    service.getIncomeDataChart(filter)
    .then(response => response.json())
    .then((data) => {
      let datasetIndex = 0 // index of dataset to update data
      incomeChart.set(data.labels, datasetIndex, data.values)
      this.incomeChart =  incomeChart.config()

      new Chart($("#incomeChart"), {
        type: 'line',
        data: this.incomeChart
      })
    })

    service.getProductionDataChart(filter)
    .then(response => response.json())
    .then((data) => {
      productionChart.set(data.labels, 0, data.values[0])
      productionChart.setValues(1, data.values[1])
      this.productionChart = productionChart.config()

      new Chart($("#productionChart"), {
        type: 'bar',
        data: this.productionChart,
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          }
        }
      })
    })

    service.getHeaderTotals(filter)
    .then(response => response.json())
    .then((data) => {
      this.headerTotals = data
    })

    service.getContributionTruckDataChart(filter)
    .then(response => response.json())
    .then((data) => {
      this.updateTruckCharts(data, this.contributionTruckInfo, 'truckChart', 'doughnut')
    })

    service.getTruckExpensesDataChart(filter)
    .then(response => response.json())
    .then((data) => {
      this.updateTruckCharts(data, this.truckExpensesInfo, 'truckChart2', 'pie')
    })
  }

  updateTruckCharts(data, info, chartId, chartType) {
    var total = 0;
    for(var i=0; i<data.values.length; i++) { total += data.values[i] };
    for(var i = 0; i < data.labels.length; i++) {
      info.push({
        name: data.labels[i],
        percentage: data.values[i] / total,
        presentationColor: backgroundColor[i]
      });
    }

    let truckChart = {
      labels: data.labels,
      datasets: [{
        data: data.values,
        backgroundColor: backgroundColor,
        hoverBackgroundColor: hoverBackgroundColor
      }]
    };

    let context = $('#' + chartId)

    if(info.length) {
      context.parents("table:first").show();
      new Chart(context, {
        type: chartType,
        data: truckChart,
        options: { legend: { display: false } }
      })
    } else {
      context.parents("table:first").hide();
    }
  }

  changeFilter(evt) {
    this.configCharts(moment(evt.date))
  }
}
