import {BaseChart}  from '../charts/chart'

export class IncomeChart extends BaseChart {

  constructor() {
    let datasets = [{
      label: "Rendimentos (R$)",
      backgroundColor: "rgba(38, 185, 154, 0.31)",
      borderColor: "rgba(38, 185, 154, 0.7)",
      pointBorderColor: "rgba(38, 185, 154, 0.7)",
      pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointBorderWidth: 1,
      data: []
    }]

    super([], datasets)
  }

}
