export class BaseChart{
    constructor(labels, datasets) {
        this.labels = labels
        this.datasets = datasets
    }

    addDataSet(dataSet) {
        this.datasets.push(dataSet)
    }

    newDataSet(label, red, green, blue) {
      return {
              label: label,
              backgroundColor: "rgba("+red+", "+green+", "+blue+", 0.31)",
              borderColor: "rgba("+red+", "+green+", "+blue+", 0.7)",
              pointBorderColor: "rgba("+red+", "+green+", "+blue+", 0.7)",
              pointBackgroundColor: "rgba("+red+", "+green+", "+blue+", 0.7)",
              pointHoverBackgroundColor: "#fff",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointBorderWidth: 1,
              data: []
          }
    }

    set(labels, datasetIndex, values) {
      this.labels = labels
      this.setValues(datasetIndex, values)
    }

    setValues(datasetIndex, values) {
      this.datasets[datasetIndex].data = values
    }

    config() {
        return {
            labels: this.labels,
            datasets: this.datasets
        }
    }

    update(elementId, type, data) {
      new Chart($('#'+elementId), {
        type: type,
        data: data
      });
    }
}
