import {BaseChart} from '../charts/chart'

export class ProductionChart extends BaseChart {
    constructor() {
        let datasets = [
                {
                    label: "Viagens Estimadas",
                    backgroundColor: "rgba(38, 185, 154, 0.31)",
                    borderColor: "rgba(38, 185, 154, 0.7)",
                    data: []
                },
                {
                    label: "Viagens Realizadas",
                    backgroundColor: "rgba(3, 88, 106, 0.3)",
                    borderColor: "rgba(3, 88, 106, 0.70)",
                    data: []
                }
            ];

        super([], datasets);
    }
}
