import {inject}         from 'aurelia-framework'
import {DialogService}  from 'aurelia-dialog'
import {RevenuesService} from 'services/revenues_service'
import {MessageService} from 'services/message_service'
import {ConfirmModal}   from 'components/confirm-modal'
import {BasePage}       from 'pages/base_page'

let service = {}
let dialogService = {}
let erros = []

@inject(RevenuesService, DialogService, MessageService)
export class Revenues extends BasePage {

  constructor(RevenuesService, DialogService, MessageService) {
    super(MessageService)

    service = RevenuesService
    dialogService = DialogService

    this.init()

  }

  init() {
    this.revenue = {}

    service.findAll()
    .then(response => response.json())
    .then(data => {
      this.revenues = data
      this.loadCompleted();
    })

    service.findAllContracts()
    .then(response => response.json())
    .then(data => {
      this.contracts = []
      for (var i = 0; i < data.length; i++) {
        this.contracts.push({
          label: data[i].name + ' - ' + data[i].contractingCompany
          , value: data[i]._id
        })
      }
    })

    this.loadTrucks({})
  }

  loadTrucks(query) {
    service.findAllTrucks(query)
    .then(response => response.json())
    .then(data => {
      this.trucks = []
      for (var i = 0; i < data.length; i++) {
        this.trucks.push({
          label: data[i].plate
          , value: data[i]._id
        })
      }
    })
  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.revenue = data
        this.refreshCustomComponents()
      })
    }
  }

  save() {
    service.save(this.revenue)
    .then(response => response.json())
    .then(data => {
      this.clear()
      this.messageService.showSuccess('Operação realizada com sucesso.')
    })
    .catch(err => {
      this.messageService.showError('Ops... ocorreu um erro durante a operação.')
    })
  }

  clear() {
    window.location.href = '#/revenues'
    this.init()
    $('select2').find('select:first').val(null).trigger('change')
  }

  fireChangeContract(event) {
    this.revenue.contract = event.detail.value
    this.loadTrucks(this.revenue.contract)
  }

  fireChangeDate(evt) {
    this.revenue.date = evt.detail.value
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          this.messageService.showSuccess('Registro removido com sucesso.')
        })
      }
    });
  }

}
