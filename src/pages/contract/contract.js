import {inject}           from 'aurelia-framework'
import {ContractService}  from 'services/contract_service'
import {LocationService}  from 'services/location_service'
import {MessageService}   from 'services/message_service'
import mapsapi from 'google-maps-api';

const requiredStepTemplate = '<i class="fa fa-warning" style="color: yellow;" title="Campos obrigatórios"></i>'

let service = {}, locationService = {}, messageService = {}

 var placeSearch, autoCompleteFrom, autoCompleteTo;

@inject(ContractService, LocationService, MessageService, mapsapi('AIzaSyBd5s-wv6FSPKbTWk1bL48HeALFhyvi40o', ['places']))
export class Contract {


  constructor(ContractService, LocationService, MessageService) {
    service = ContractService
    locationService = LocationService
    messageService = MessageService

    this.contract = {
      trucks: []
      , employees: []
    }

    service.findTrucks()
    .then(response => response.json())
    .then(data => {
      this.trucks = data
    })

    service.findEmployees()
    .then(response => response.json())
    .then(data => {
      this.employees = data
    })

    var $this = this

    setTimeout(() => {
      autoCompleteFrom = new google.maps.places.Autocomplete(
        ($('input.placesFrom').get(0)),
        {types: ['geocode']});

      autoCompleteFrom.addListener('place_changed', () => {
        let locationFrom = autoCompleteFrom.getPlace().geometry.location;
        $this.contract.locationFrom = [ locationFrom.lat(), locationFrom.lng()]
      });

      autoCompleteTo = new google.maps.places.Autocomplete(
        ($('input.placesTo').get(0)),
        {types: ['geocode']});

      autoCompleteTo.addListener('place_changed', () => {

          let locationTo = autoCompleteTo.getPlace().geometry.location;
          $this.contract.locationTo = [ locationTo.lat(), locationTo.lng()]
      });
    }, 300);

  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.contract = data

        if(this.contract.trucks === undefined) {
          this.contract.trucks = []
        }
        if(this.contract.employees === undefined) {
          this.contract.employees = []
        }

        setTimeout(() => {
          $("#estimatedProduction").data("ionRangeSlider").update({
            from: this.contract.estimatedProduction
          });
        }, 10)
      })
    }
  }

  attached() {

    let $this = this

    $('#wizard').smartWizard({
      labelNext: 'Próximo'
      , labelPrevious: 'Anterior'
      , labelFinish: 'Concluir'
      , keyNavigation: false
      , reverseButtonsOrder: true
      , includeFinishButton : false
      , onLeaveStep: (obj, context) => {
        $('a[href="#step-'+context.toStep+'"]').find('span:first').html(context.toStep);
        return true
      }
      , onFinish: (objs, context) => {
        this.save()
      }
    })

    $('#wizard_verticle').smartWizard({
      transitionEffect: 'slide'
    });
    $('#wizard_verticle').smartWizard("fixHeight", "None");

    $('.buttonNext').addClass('btn btn-default')
    $('.buttonPrevious').addClass('btn btn-default')
    $('.buttonFinish').addClass('btn btn-success')

    $('#estimatedProduction').ionRangeSlider({
      min: 0
      , max: 1000
      , grid: true
      , onChange: (data) => {
        $this.contract.estimatedProduction = data.from
      }
    });

  }

  save() {
    let error = false
    if(!$('form').parsley().isValid()) {
      $('a[href="#step-1"]').find('span:first').html(requiredStepTemplate);
      error = true
    }
    if(!this.contract.employees.length) {
      $('a[href="#step-2"]').find('span:first').html(requiredStepTemplate);
      error = true
    }
    if(!this.contract.trucks.length) {
      $('a[href="#step-3"]').find('span:first').html(requiredStepTemplate);
      error = true
    }
    if(!error) {
      service.save(this.contract)
      .then(response => response.json())
      .then(data => {
        window.location.href = '#/contracts'
      })
    } else {
      messageService.showError('Existem campos obrigatórios não preenchidos.')
    }
  }

  changeDate(evt) {
    this.contract.date = evt.detail.value
  }

  addEmployeeToContract(evt) {
    let id = evt.detail.value
    let index = this.contract.employees.indexOf(id)
    index === -1 ? this.contract.employees.push(id) : this.contract.employees.splice(index, 1)
  }

  addTruckToContract(evt) {
    let id = evt.detail.value
    let index = this.contract.trucks.indexOf(id)
    index === -1 ? this.contract.trucks.push(id) : this.contract.trucks.splice(index, 1)
  }
}
