import {inject}          from 'aurelia-framework'
import {ContractService} from 'services/contract_service'
import {MessageService}  from 'services/message_service'
import {BasePage}        from 'pages/base_page'

import mapsapi from 'google-maps-api';

let service = {}, idMap = []

var placeSearch, autoCompleteFrom, autoCompleteTo;

@inject(ContractService, MessageService, mapsapi('AIzaSyBd5s-wv6FSPKbTWk1bL48HeALFhyvi40o', ['places']))
export class Budget extends BasePage {

  constructor(ContractService, MessageService) {

    super(MessageService)

    service = ContractService

    this.budget = {
      costs: []
    }

    setTimeout(() => {
        autoCompleteFrom = new google.maps.places.Autocomplete(
        ($('input.placesFrom').get(0)),
        {types: ['geocode']});

        autoCompleteFrom.addListener('place_changed', () => {
          this.budget.placeFrom = autoCompleteFrom.getPlace().formatted_address;
        });

        autoCompleteTo = new google.maps.places.Autocomplete(
          ($('input.placesTo').get(0)),
          {types: ['geocode']});

          autoCompleteTo.addListener('place_changed', () => {
            this.budget.placeTo = autoCompleteTo.getPlace().formatted_address;
          });
        }, 200);
      }

      geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autoCompleteFrom.setBounds(circle.getBounds());
          });
        }
      }


      loadCitiesFrom(evt) {
        this.citiesFrom = []
        if(evt.detail.value) {
          this.budget.fromState = evt.detail.value
          locationService.findCities(idMap[evt.detail.value])
          .then(response => response.json())
          .then(data => {
            this.citiesFrom = convertLocationsToOptions(data)
          })
        }
      }

      loadCitiesTo(evt) {
        this.citiesTo = []
        if(evt.detail.value) {
          this.budget.toState = evt.detail.value
          locationService.findCities(idMap[evt.detail.value])
          .then(response => response.json())
          .then(data => {
            this.citiesTo = convertLocationsToOptions(data)
          })
        }
      }

      changeFromCity(evt) {
        this.budget.fromCity = evt.detail.value
      }

      changeToCity(evt) {
        this.budget.toCity = evt.detail.value
      }

      changeTotalValue(evt) {
        this.budget.totalValue = evt.detail.value
      }

      changeFuelPrice(evt) {
        this.budget.fuelPrice = evt.detail.value
      }

      changeAverageConsumption(evt) {
        this.budget.averageConsumption = evt.detail.value
      }

      changeAdicionalValue(evt, panel) {
        panel.value = evt.detail.value
      }

      addAditionalCost() {
        $('.panel-collapse').removeClass('in')
        this.budget.costs.push({
          title: '#'+(this.budget.costs.length + 1)
          , value: 0.0
        })
      }

      removeAditionalCost(index) {
        this.budget.costs.splice(index, 1)
      }

      calc() {
        service.findDistanceInfo(this.budget)
        .then(response => response.json())
        .then(data => {
          this.result = data
        })
      }
    }
