import {inject}          from 'aurelia-framework';
import {ContractService} from 'services/contract_service';
import {DialogService}   from 'aurelia-dialog'
import {ConfirmModal}    from 'components/confirm-modal'

let dialogService = {}, service = {}

@inject(ContractService, DialogService)
export class ContractList {

  constructor(ContractService, DialogService) {

    dialogService = DialogService
    service = ContractService

    service.findAll()
    .then(response => response.json())
    .then(data => {
      this.contracts = data
    })
  }

  attached() {
    setTimeout(() => {
      if ($(".progress .progress-bar")[0]) {
        $('.progress .progress-bar').progressbar()
      }
    }, 100)
  }

  getPhoto(id) {
    return id ? service.getPublic('images/' + id) : 'src/images/user.png'
  }

  changeStatus(contract) {

    let status = !contract.active ? 'Ativação' : 'Desativação'

    dialogService.open({viewModel: ConfirmModal, model: 'Confirma a ' + status + ' deste frete?' }).then(response => {
      if (!response.wasCancelled) {
        contract.active = !contract.active
        service.save(contract)
      }
    });
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          service.findAll()
          .then(data => {
          })
        })
      }
    });
  }
}
