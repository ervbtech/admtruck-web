import {inject}          from 'aurelia-framework';
import {ContractService} from 'services/contract_service';

let service = {}

@inject(ContractService)
export class ContractDetail {

  constructor(ContractService) {
    service = ContractService
  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.contract = data
      })
    }
  }

  getExtensionFile(name) {
    return name ? name.substr(name.indexOf('\.') + 1) : ''
  }

  // hasAttachments() {
  //   return this.contract.attachments !== undefined && this.contract.attachments.length > 0
  // }

}
