import {inject}           from 'aurelia-framework'
import {DialogService}    from 'aurelia-dialog'
import {EmployeeService}  from 'services/employee_service'

let service = {}, dialogService = {}

@inject(EmployeeService, DialogService)
export class EmployeeProfile {

  constructor(EmployeeService, DialogService) {
    service = EmployeeService
    dialogService = DialogService
  }

  activate(params) {

    service.findById(params.id)
    .then(response => response.json())
    .then(data => {
      this.employee =  data
    })
  }

  attached() {
    setTimeout(() => {
      if ($(".progress .progress-bar")[0]) {
        $('.progress .progress-bar').progressbar()
      }
    }, 100)
  }

  getPhoto(id) {
    return service.getPublic('images/' + id)
  }

}
