import {inject}             from 'aurelia-framework'
import {EmployeeService}    from 'services/employee_service'
import {AttachmentService}  from 'services/attachment_service'
import {MessageService}     from 'services/message_service'
import {DialogService}      from 'aurelia-dialog'
import {CropperModal}       from 'components/custom/cropper/cropper-modal'
import moment               from 'moment'
import {BasePage}           from 'pages/base_page'

let service = {}, dialogService = {}, attachmentService = {}

let photo = {}


@inject(EmployeeService, DialogService, AttachmentService, MessageService)
export class Employee extends BasePage {
  constructor(EmployeeService, DialogService, AttachmentService, MessageService) {

    super(MessageService)

    this.titleFlow = 'Cadastrar'
    this.employee = {
      birthDate: moment()
    }
    this.emailConfirmation = ''
    service = EmployeeService
    attachmentService = AttachmentService
    dialogService = DialogService
  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.employee = data
        this.emailConfirmation = this.employee.email
      })
    }
  }

  save() {
    service.save(this.employee)
    .then(response => response.json())
    .then(data => {
      attachmentService.upload([{ file: photo, name: data._id }])
      .then(data => {
          this.messageService.showSuccess('Operação realizada com sucesso.')
          window.location.href = '#/employees'
      })
    })
    .catch((err) => {
      this.messageService.showError('Ops... ocorreu um erro durante a operação.')
    })
  }

  fireChangeDate(evt) {
    this.employee.birthDate = evt.detail.value
  }

  getPhoto(id) {
    return id ? service.getPublic('images/' + id) : 'src/images/user.png'
  }

  showCropperModal() {

    var $image = $('.img-responsive.avatar-view')

    dialogService.open({viewModel: CropperModal, model: { image: $image.attr('src') }}).then(response => {
      if (!response.wasCancelled) {
        var blobBin = atob(response.output.split(',')[1]);
        var array = [];
        for(var i = 0; i < blobBin.length; i++) {
          array.push(blobBin.charCodeAt(i));
        }

        photo = new Blob([new Uint8Array(array)], {type: 'image/png'});

        $image.attr('src', response.output)
      }
    });
  }

}
