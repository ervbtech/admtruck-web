import {inject}          from 'aurelia-framework';
import {EmployeeService} from 'services/employee_service';
import {DialogService}   from 'aurelia-dialog'
import {ConfirmModal}    from 'components/confirm-modal'

let dialogService = {}, service = {}

@inject(EmployeeService, DialogService)
export class EmployeeList {

  constructor(EmployeeService, DialogService) {

    dialogService = DialogService
    service = EmployeeService
    EmployeeService.findAll()
    .then(response => response.json())
    .then(data => {
      this.employees = data
    })
  }

  getPhoto(id) {
    return id ? service.getPublic('images/' + id) : 'src/images/user.png'
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          service.findAll()
          .then(response => response.json())
          .then(data => {
            this.employees = data
          })
        })
      }
    });
  }
}
