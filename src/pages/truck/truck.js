import {inject}       from 'aurelia-framework'
import {TruckService} from 'services/truck_service'

let service = {}

@inject(TruckService)
export class Truck {
  constructor(TruckService) {
    this.titleFlow = 'Cadastrar'
    this.truck = {
        status: 'active'
      , participationPercentage: 0
    }

    service = TruckService
  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.titleFlow = 'Alterar'
        this.truck = data
        setTimeout(() => {
          $('.knob').val(this.truck.participationPercentage).trigger('change')
          $('switchery').trigger('change')
        }, 10);
      })
    }
  }

  fireChangeAggregation(evt) {
    this.truck.forAggregation = evt.target.checked
  }

  fireChangeParticipationExpense(evt) {
    this.truck.participationExpense = evt.target.checked
  }

  attached() {
    var $this = this

    // jQuery Knob
    $(".knob").knob({
      release: function(value) {
        $this.truck.participationPercentage = value
      }, draw: function() {

      // "tron" case
      if (this.$.data('skin') == 'tron') {

        this.cursorExt = 0.3;

        var a = this.arc(this.cv) // Arc
        ,
        pa // Previous arc
        , r = 1;

        this.g.lineWidth = this.lineWidth;

        if (this.o.displayPrevious) {
          pa = this.arc(this.v);
          this.g.beginPath();
          this.g.strokeStyle = this.pColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
          this.g.stroke();
        }

        this.g.beginPath();
        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
        this.g.stroke();

        this.g.lineWidth = 2;
        this.g.beginPath();
        this.g.strokeStyle = this.o.fgColor;
        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
        this.g.stroke();

        return false;
      }
    }
  });
}

save() {
  service.save(this.truck)
  .then(data => {
    window.location.href = '#/trucks'
  })
}
}
