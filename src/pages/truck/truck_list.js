import {inject}         from 'aurelia-framework'
import {TruckService}   from 'services/truck_service'
import {DialogService}  from 'aurelia-dialog'
import {ConfirmModal}   from 'components/confirm-modal'

let dialogService = {}
let service       = {}

@inject(TruckService, DialogService)
export class TruckList {
  constructor(TruckService, DialogService) {

    dialogService = DialogService
    service = TruckService

    service.findAll()
    .then(response => response.json())
    .then((data) => {
      this.trucks = data
    })
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          service.findAll()
            .then(data => {
            })
        })
      }
    });
  }
}
