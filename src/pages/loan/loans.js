import {inject}         from 'aurelia-framework'
import {DialogService}  from 'aurelia-dialog'
import {LoansService}   from 'services/loans_service'
import {MessageService} from 'services/message_service'
import {ConfirmModal}   from 'components/confirm-modal'
import {BasePage}       from 'pages/base_page'

let service = {}
let dialogService = {}

@inject(LoansService, DialogService, MessageService)
export class Loans extends BasePage {

  constructor(LoansService, DialogService, MessageService) {
    super(MessageService)

    service = LoansService
    dialogService = DialogService
    this.loan = { }
    service.findAll()
    .then(response => response.json())
    .then(data => {
      this.loans = data
      this.loadCompleted();
    })
  }

  activate(params) {
    if(params.id) {
      service.findById(params.id)
      .then(response => response.json())
      .then(data => {
        this.loan = data
      })
    }
  }

  save() {
    service.save(this.loan)
    .then(response => response.json())
    .then(data => {
      service.findAll()
      .then(response => response.json())
      .then(data => {
        this.loans = data
        this.messageService.showSuccess('Operação realizada com sucesso.')
        this.clear()
      })
    })
    .catch(err => {
      this.messageService.showError('Ops... ocorreu um erro durante a operação.')
    })
  }

  clear() {
    this.loan = {}
    window.location.href = '#/loans'
    $('select2').find('select:first').val(null).trigger('change')
  }

  fireChangeDate(evt) {
    this.loan.date = evt.detail.value
  }

  fireChangeValue(event) {
    this.loan.value = event.detail.value
  }

  fireChangeTax(event) {
    this.loan.tax = event.detail.value
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          this.messageService.showSuccess('Registro removido com sucesso.')
        })
      }
    });
  }

}
