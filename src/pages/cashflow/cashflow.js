'use strict'
import {inject}           from 'aurelia-framework'
import {DashboardService} from 'services/dashboard_service'
import {BaseChart}            from '../dashboard/charts/chart'
import moment             from 'moment'

import 'chart.js'
import 'moment/locale/pt-br'

let service = {}, chartUtil = {}

@inject(BaseChart, DashboardService)
export class CashFlow {

  constructor(BaseChart, DashboardService) {
    service = DashboardService
    chartUtil = BaseChart
  }

  attached() {
    this.yearFilter = moment().year()
    this.search()
  }

  search() {
    this.cashflowChart = {
        labels: [],
        datasets: []
    }

    service.getCashFlow(this.yearFilter)
      .then(response => response.json())
      .then(data => {
        let cashflow = data
        this.flows = data[3]
        let labels = []
        let revenues = chartUtil.newDataSet('Rendimentos (R$)', 38, 185, 154)
        let expense  = chartUtil.newDataSet('Despesas (R$)', 217, 83, 79)
        let loans    = chartUtil.newDataSet('Empréstimos (R$)', 91, 192, 222)

        for(let i = 0; i < 12; i++) {

            labels.push(moment.monthsShort()[i])

            expense.data.push(-cashflow[0][i].value)
            revenues.data.push(cashflow[1][i].value)
            loans.data.push(cashflow[2][i].value)

        }

        this.cashflowChart = {
            labels: labels,
            datasets: [revenues, expense, loans]
        }

        new Chart($("#cashflowChart"), {
          options: {
              responsive: true,
              maintainAspectRatio: false
          },
          type: 'line',
          data: this.cashflowChart
        })
      })
  }

}
