'use strict'

import 'bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR'

/* JQUERY PLUGINS */
import maskmoney         from 'plentz/jquery-maskmoney/dist/jquery.maskMoney.min'

export class BasePage {

  constructor(MessageService, Enviroment) {
    this.messageService = MessageService
    this.env = Enviroment
  }

  attached() {

    let $this = this

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip({
      container: 'body'
    });
    // /Tooltip

    $('form .btn[type="submit"]').on('click', function() {
      $('form').parsley().validate();
      validateFront();
    });
    var validateFront = function() {
      if (true === $('form').parsley().isValid()) {
        $('.bs-callout-info').removeClass('hidden');
        $('.bs-callout-warning').addClass('hidden');
      } else {
        $('.bs-callout-info').addClass('hidden');
        $('.bs-callout-warning').removeClass('hidden');
        $this.messageService.showError('Existem campos obrigatórios não preenchidos.')
      }
    };

    $('form').parsley().on('form:validate', (formInstance) => {
      var ok = formInstance.isValid()
      let components = $('[data-parsley-id]')
      if(!ok) {
        for(var i = 0; i < components.length; i++) {
          let component = $(components[i])
          if(component.is('select') && component.hasClass('parsley-error')) {
            component.parent().find('span.select2-selection').addClass('parsley-error')
          }
        }
      } else {
        for(var i = 0; i < components.length; i++) {
          let component = $(components[i])
          if(component.is('select') && component.hasClass('parsley-success')) {
            component.parent().find('span.select2-selection').removeClass('parsley-error')
          }
        }
      }
    });

  }

  loadCompleted() {
    setTimeout(() => {
      $('table').dataTable({
        destroy: true,
        language: {
          url: 'src/lib/dataTables.ptBR.lang.json'
        }
      });
      $('table').on( 'draw.dt', (e, settings, data) => {
        $('.dataTables_filter').find('input:first').addClass('form-control')
        $('.dataTables_length').hide()
      });

      $('.maskMoney').each(function(){ // function to apply mask on load!
          $(this).maskMoney('mask', $(this).val());
      })
    }, 100);
  }

  refreshCustomComponents() {
    setTimeout(() => {
      $('select').trigger('change')
    }, 10);
  }

}
