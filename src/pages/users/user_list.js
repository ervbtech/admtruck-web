import {inject}         from 'aurelia-framework'
import {UserService}   from 'services/user_service'
import {DialogService}  from 'aurelia-dialog'
import {ConfirmModal}   from 'components/confirm-modal'

let dialogService = {}
let service       = {}

@inject(UserService, DialogService)
export class UserList {
  constructor(UserService, DialogService) {

    dialogService = DialogService
    service = UserService

    service.findAll()
    .then(response => response.json())
    .then((data) => {
      this.users = data
    })
  }

  confirmDelete(id) {
    dialogService.open({viewModel: ConfirmModal, model: 'Deseja remover o registro selecionado?' }).then(response => {
      if (!response.wasCancelled) {
        service.delete(id)
        .then(() => {
          service.findAll()
            .then(data => {
            })
        })
      }
    });
  }

  changeStatus(user) {

    let status = !user.active ? 'Ativação' : 'Inativação'

    dialogService.open({viewModel: ConfirmModal, model: 'Confirma a ' + status + ' deste usuário?' }).then(response => {
      if (!response.wasCancelled) {
        user.active = !user.active
        service.save(user)
      }
    });
  }
}
