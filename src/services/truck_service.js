'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {BaseService}        from 'services/base_service'

@inject(HttpClient, AuthService, Enviroment)
export class TruckService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment) {
    super(HttpClient, Enviroment, 'trucks/', AuthService)
  }

}
