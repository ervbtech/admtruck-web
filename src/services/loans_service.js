'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {ContractService}    from 'services/contract_service'
import {TruckService}       from 'services/truck_service'
import {BaseService}        from 'services/base_service'

let serviceContract = {}, serviceTruck = {}

@inject(HttpClient, AuthService, Enviroment, ContractService, TruckService)
export class LoansService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment, ContractService, TruckService) {
      super(HttpClient, Enviroment, 'loans/', AuthService)
      serviceContract = ContractService
      serviceTruck = TruckService
  }

  findAllContracts() {
    return serviceContract.findByQuery({ status: 'active' })
  }

  findAllTrucks() {
    return serviceTruck.findAll()
  }

}
