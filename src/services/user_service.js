'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {BaseService}        from 'services/base_service'

@inject(HttpClient, AuthService, Enviroment)
export class UserService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment) {
      super(HttpClient, Enviroment, 'users/', AuthService)
  }

  getPicture(userId) {
    return this.httpClient.fetch(this.env.getUri(this.path + 'pic/' + userId))
  }

}
