'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {ContractService}    from 'services/contract_service'
import {TruckService}       from 'services/truck_service'
import {EmployeeService}    from 'services/employee_service'
import {LoansService}       from 'services/loans_service'
import {BaseService}        from 'services/base_service'

let serviceContract = {}, serviceTruck = {}, serviceEmployee = {}, serviceLoan = {}

@inject(HttpClient, AuthService, Enviroment, ContractService, TruckService, EmployeeService, LoansService)
export class ExpenseService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment, ContractService, TruckService, EmployeeService, LoansService) {
      super(HttpClient, Enviroment, 'expenses/', AuthService)
      serviceContract = ContractService
      serviceTruck = TruckService
      serviceEmployee = EmployeeService
      serviceLoan = LoansService
  }

  findAllContracts() {
    return serviceContract.findByQuery({ active: true })
  }

  findAllTrucks(query) {
    return serviceContract.findTrucks(query)
  }

  findAllEmployees() {
    return serviceEmployee.findAll()
  }

  findAllLoans() {
    return serviceLoan.findAll()
  }

}
