'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {BaseService}        from 'services/base_service'
import {UserService}        from 'services/user_service'

let userService = {}

@inject(HttpClient, AuthService, Enviroment, UserService)
export class EmployeeService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment, UserService) {
      super(HttpClient, Enviroment, 'employees/', AuthService)
      userService = UserService
  }

  getPicture(userId) {
    return userService.getPicture(userId)
  }

}
