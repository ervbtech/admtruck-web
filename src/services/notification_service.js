'use strict'

import {inject}           from 'aurelia-framework'
import {HttpClient, json} from 'aurelia-fetch-client'
import {AuthService}      from 'aurelia-auth'
import {Enviroment}       from 'utils/enviroment'

const path     = 'notifications/'
let httpClient = {}, env = {}

@inject(HttpClient, AuthService, Enviroment)
export class NotificationService {

  constructor(HttpClient, AuthService, Enviroment) {
    httpClient = HttpClient
    httpClient.configure(config => {
          config.withInterceptor(AuthService.tokenInterceptor)
    })
    env = Enviroment
  }

  find(query) {
    return httpClient.fetch(env.getUri(path + '?q=' + JSON.stringify(query)))
  }

  save(entity) {
    let method = 'POST'
      , savePath = path
      , config = {
          method: '',
          body: {},
          headers: {
            'Content-Type': 'application/json'
          }
      }

    if(entity._id) {
      method = 'PUT'
      savePath += entity._id
    }

    config.method = method
    config.body = JSON.stringify(entity)
    return httpClient.fetch(env.getUri(savePath), config)
  }

}
