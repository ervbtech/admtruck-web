'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {TruckService}       from 'services/truck_service'
import {EmployeeService}    from 'services/employee_service'
import {BaseService}        from 'services/base_service'

let serviceTruck = {}, serviceEmployee = {}

@inject(HttpClient, AuthService, Enviroment, TruckService, EmployeeService)
export class ContractService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment, TruckService, EmployeeService) {
    super(HttpClient, Enviroment, 'contracts/', AuthService)

    serviceTruck = TruckService
    serviceEmployee = EmployeeService
  }

  findTrucks(query) {
    return query && query.length ? this.findTrucksAssociated(query) : serviceTruck.findAll()
  }

  findTrucksAssociated(query) {
    return this.httpClient.fetch(this.env.getUri(this.path + 'trucks/' + query))
  }

  findEmployees() {
    return serviceEmployee.findAll()
  }

  findDistanceInfo(params) {
    let path = this.path + 'budget'
    , config = {
      method: 'POST',
      body: JSON.stringify(params),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return this.httpClient.fetch(this.env.getUri(path), config)
  }

}
