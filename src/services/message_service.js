'use strict'

import PNotify      from 'node_modules/gentelella/vendors/pnotify/dist/pnotify'
import pnotifybtns  from 'node_modules/gentelella/vendors/pnotify/dist/pnotify.buttons'

export class MessageService {

  showError(message) {
    new PNotify({
        title: 'Erro :(',
        text: message,
        type: 'error',
        styling: 'bootstrap3'
    })
  }

  showInfo(message) {
    new PNotify({
        title: 'Aviso',
        text: message,
        type: 'info',
        styling: 'bootstrap3'
    })
  }

  showSuccess(message) {
    new PNotify({
        title: 'Sucesso',
        text: message,
        type: 'success',
        styling: 'bootstrap3'
    })
  }

  showWarning(message) {
    new PNotify({
        title: 'Atenção',
        text: message,
        styling: 'bootstrap3'
    })
  }

  showDark(message) {
    new PNotify({
        title: 'Aviso',
        text: message,
        type: 'info',
        styling: 'bootstrap3',
        addclass: 'dark'
    })
  }

}
