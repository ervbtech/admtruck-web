'use strict'

export class BaseService {

  constructor(HttpClient, Enviroment, path, auth) {
    let srv = this
    srv.path = path
    srv.auth = auth
    srv.env = Enviroment
    srv.httpClient = HttpClient
    srv.httpClient.configure(config => {
      config
      .withInterceptor(auth.tokenInterceptor)
      .withInterceptor({
        request(request) {
          srv.env.requesting = true
          if(auth.isAuthenticated()) {
            if (request.headers.has('company'))
  					{
  						request.headers.delete('company');
  					}
            request.headers.append('company', auth.getTokenPayload().data.company._id);
            return request
          } else {
            auth.logout('/login.html');
          }
        },
        response(response) {
          srv.env.requesting = false
          return response
        }
      })
    })
  }

  buildPath(path) {
    return this.httpClient.fetch(this.env.getUri(this.path + path))
  }

  getPublic(subpath) {
    return this.env.getUri('public/' + subpath)
  }

  getPhoto(id) {
    return id ? userService.getPublic('images/' + id) : 'src/images/user.png'
  }

  findAll() {
    return this.httpClient.fetch(this.env.getUri(this.path))
  }

  findByQuery(query, customPathFilter) {
    let path = this.path + (customPathFilter ? customPathFilter : 'filter')
    , config = {
      method: 'POST',
      body: JSON.stringify(query),
      headers: {
        'Content-Type': 'application/json'
      }
    }

    return this.httpClient.fetch(this.env.getUri(path), config)
  }

  findById(id) {
    return this.httpClient.fetch(this.env.getUri(this.path + id))
  }

  save(entity) {
    let method = 'POST'
    , savePath = this.path
    , config = {
      method: '',
      body: {},
      headers: {
        'Content-Type': 'application/json'
      }
    }

    if(entity._id) {
      method = 'PUT'
      savePath += entity._id
    }

    entity.company = this.auth.getTokenPayload().data.company._id

    config.method = method
    config.body = JSON.stringify(entity)
    return this.httpClient.fetch(this.env.getUri(savePath), config)
  }

  delete(id) {
    return this.httpClient.fetch(this.env.getUri(this.path + id), { method: 'DELETE' })
  }

}
