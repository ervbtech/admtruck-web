'user strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'
import {BaseService}        from 'services/base_service'

@inject(HttpClient, AuthService, Enviroment)
export class AttachmentService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment) {
      super(HttpClient, Enviroment, 'attachments/', AuthService)
  }

  upload(files) {
    let formData = new FormData();

    for (let i = 0; i < files.length; i++) {
      let data = files[i]
      formData.append('file', data.file, data.name);
    }
    return this.httpClient.fetch(this.env.getUri(this.path + 'upload'), {
        method: 'POST',
        body: formData
      })
  }

}
