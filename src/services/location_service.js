'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {BaseService}        from 'services/base_service'

@inject(HttpClient, AuthService, Enviroment)
export class LocationService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment) {
    super(HttpClient, Enviroment, 'locations/', AuthService)
  }

  findCountries() {
      return this.httpClient.fetch(this.env.getUri(this.path + 'countries'))
  }

  findStates(country) {
    let aditionalPath = country ? '/' + country : ''
    return this.httpClient.fetch(this.env.getUri(this.path + 'states' + aditionalPath))
  }

  findCities(state) {
    return this.httpClient.fetch(this.env.getUri(this.path + 'cities' + '/' + state))
  }

}
