'use strict'

import {inject}             from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {AuthService}        from 'aurelia-auth'
import {Enviroment}         from 'utils/enviroment'

import {BaseService}        from 'services/base_service'

@inject(HttpClient, AuthService, Enviroment)
export class DashboardService extends BaseService {

  constructor(HttpClient, AuthService, Enviroment) {
    super(HttpClient, Enviroment, 'dashboard/', AuthService)
  }

  getCashFlow(yearFilter) {
    return this.buildPath('cashflow/' + yearFilter)
  }

  getHeaderTotals(filter) {
    return this.buildPath('header/' + filter)
  }

  getIncomeDataChart(filter) {
    return this.buildPath('income/' + filter)
  }

  getProductionDataChart(filter) {
    return this.buildPath('production/' + filter)
  }

  getContributionTruckDataChart(filter) {
    return this.buildPath('contributiontruck/' + filter)
  }

  getTruckExpensesDataChart(filter) {
    return this.buildPath('truckexpenses/' + filter)
  }

  getContractExpensesDataChart(filter) {
    return this.buildPath('contract/expenses/' + filter)
  }

  getContractRevenuesDataChart(filter) {
    return this.buildPath('contract/revenues/' + filter)
  }
}
