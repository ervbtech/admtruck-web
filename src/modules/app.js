import {AuthorizeStep}  from 'aurelia-auth'
import {inject}         from 'aurelia-framework'
import {Gentelella}     from 'lib/gentelella'
import {Enviroment}     from '../utils/enviroment'

let gentelella = {}

@inject(Enviroment, Gentelella)
export class App {

  constructor(Enviroment, Gentelella) {
    this.env = Enviroment
    gentelella = Gentelella
  }

  configureRouter(config, router) {
    this.router = router

    // Add a route filter to the authorize extensibility point.
    config.addPipelineStep('authorize', AuthorizeStep)

    config.mapUnknownRoutes(instruction => {
      window.location = '/login.html'
    })

    config.map([
      { route: '', redirect: 'dashboard' }
      /* CONTRACT ROUTES */
      , { route: 'contracts', name: 'Contract - List', moduleId: 'pages/contract/contract_list', nav: true, auth: true }
      , { route: 'contract', name: 'Contract', moduleId: 'pages/contract/contract', nav: true, auth: true }
      , { route: 'contract/:id', name: 'Contract', moduleId: 'pages/contract/contract', auth: true }
      , { route: 'contract/:id/detail', name: 'Contract Detail', moduleId: 'pages/contract/contract_detail', auth: true }
      , { route: 'budget', name: 'Budget', moduleId: 'pages/contract/budget', nav: true, auth: true }


      /* DASHBOARD ROUTES */
      , { route: 'dashboard', name: "Dashboard", moduleId: 'pages/dashboard/dashboard', nav: true, auth: true }

      /* DOCUMENTS ROUTES */
      , { route: 'documents', name: "Documents", moduleId: 'pages/documents/documents', nav: true, auth: true }

      /* EMPLOYEE ROUTES */
      , { route: 'employees', name: 'Employee - List', moduleId: 'pages/employee/employee_list', nav: true, auth: true }
      , { route: 'employee', name: 'Employee', moduleId: 'pages/employee/employee', nav: true, auth: true }
      , { route: 'employee/:id', name: 'Employee', moduleId: 'pages/employee/employee', auth: true }
      , { route: 'employee/:id/profile', name: 'Profile', moduleId: 'pages/employee/employee_profile', auth: true }

      /* EXPENSE ROUTES */
      , { route: 'expenses', name: 'Expenses', moduleId: 'pages/expense/expenses', nav: true, auth: true }
      , { route: 'expenses/:id', name: 'Expenses', moduleId: 'pages/expense/expenses', auth: true }

      /* LOAN ROUTES */
      , { route: 'loans', name: 'Loans', moduleId: 'pages/loan/loans', nav: true, auth: true }
      , { route: 'loans/:id', name: 'Loans', moduleId: 'pages/loan/loans', auth: true }

      /* REVENUES ROUTES */
      , { route: 'revenues', name: 'Revenues', moduleId: 'pages/revenues/revenues', nav: true, auth: true }
      , { route: 'revenues/:id', name: 'Revenues', moduleId: 'pages/revenues/revenues', auth: true }

      /* TRUCKS ROUTES */
      , { route: 'trucks', name: 'Truck - List', moduleId: 'pages/truck/truck_list', nav: true, auth: true }
      , { route: 'truck', name: 'Truck', moduleId: 'pages/truck/truck', nav: true, auth: true }
      , { route: 'truck/:id', name: 'Truck', moduleId: 'pages/truck/truck', auth: true }

      /* USER ROUTES */
      , { route: 'users', name: 'User - List', moduleId: 'pages/users/user_list', nav: true, auth: true }

      /* CASHFLOW ROUTES */
      , { route: 'cashflow', name: 'Cashflow View', moduleId: 'pages/cashflow/cashflow', nav: true, auth: true }

    ])
  }

  attached() {
    gentelella.init()

    // Close loading screen
    setTimeout(() => {
      window.loading_screen.finish();
    }, 3000)
  }

}
