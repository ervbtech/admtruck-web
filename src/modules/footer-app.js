import {inject}               from 'aurelia-framework'
import {AuthService}          from 'aurelia-auth'

@inject(AuthService)
export class App {
  constructor(AuthService) {
    this.user = AuthService.getTokenPayload().data
 }

}
