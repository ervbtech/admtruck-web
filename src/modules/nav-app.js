import {inject}         from 'aurelia-framework'
import {AuthService}    from 'aurelia-auth'
import {Gentelella}     from 'lib/gentelella'

@inject(AuthService, Gentelella)
export class NavApp {

  constructor(AuthService, Gentelella) {
    this.user = AuthService.getTokenPayload().data
    this.gentelella = Gentelella
  }

  attached() {
    this.gentelella.init()
  }
}
