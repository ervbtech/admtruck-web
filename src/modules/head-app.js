import {inject}               from 'aurelia-framework'
import {AuthService}          from 'aurelia-auth'
import {Enviroment}           from 'utils/enviroment'
import {NotificationService}  from 'services/notification_service'
import {UserService}          from 'services/user_service'
import moment                 from 'moment'
import io                     from 'socket.io-client'

let socket = {}
let notificationService = {}, userService = {}, auth = {}

@inject(Enviroment, NotificationService, AuthService, UserService)
export class App {
  constructor(Enviroment, NotificationService, AuthService, UserService) {

    notificationService = NotificationService
    auth = AuthService
    userService = UserService
    this.enviroment = Enviroment
    this.user = auth.getTokenPayload().data

    let q = { read: false } // query string

    notificationService.find(q)
    .then(response => response.json())
    .then(data => {
      this.notifications = data
      this.notificationCount = data.length
    })

    socket = io(Enviroment.host, {
      query: {
        company: auth.getTokenPayload().data.company._id
      }
    })
  }

  activate() {

    socket.on('connect', () => {
      window.loading_screen.finish()

      window.location.href = '#/dashboard'
    })

    socket.on('disconnect', () => {
      window.loading_screen = window.pleaseWait({
        logo: "src/images/server_down.png",
        backgroundColor: 'rgba(42, 63, 84, 0.9)',
        loadingHtml: "<p class='loading-message'><h1>Ops...&nbsp;<i class='fa fa-frown-o'></i></h1><h4>A conexão com o servidor foi interrompida, seu acesso será retomado automáticamente assim que a conexão for restaurada.<br /> Verifique sua conexão com internet. Lamentamos o inconveniente...</p><div class='spinner'></h4></div>"
      });
    })

    socket.on('notifications::'+this.user.company._id, (data) => {
      for (var i = 0; i < data.length; i++) {
        this.notificationCount++
        if(this.notifications.length == 10)
        break
        if(this.notifications.indexOf(data[i]) < 0)
        this.notifications.push(data[i])
      }
      let notifCountElement = $('#notificationCount')

      /* ANIMATE NOTIFICATION INDICATOR */
      notifCountElement.addClass('shake')
      setTimeout(() => {
        var beep = new Audio('src/medias/definite.mp3');
        beep.play();
      }, 100)
      setTimeout(() => {
        notifCountElement.removeClass('shake')
      }, 3000)

    })
  }

  getNotificationTime(date) {
    let time = ''
    let minutes = moment().diff(date, 'minutes')
    if(minutes > 60) {
      let hours = minutes / 60
      if(hours > 24) {
        time = moment(date).format('DD/MM/YYYY')
      } else {
        time = hours.toFixed(0) + ' horas atrás '
      }
    } else if (minutes < 1) {
      time = 'Agora'
    } else {
      time = minutes + ' minutos atrás '
    }
    return time
  }

  markReadNotification(notif) {
    let index = this.notifications.indexOf(notif)
    notif.read = true
    notificationService.save(notif)
    .then(response => response.json())
    .then(data => {
      this.notifications.splice(index, 1)
      this.notificationCount--
    })
  }

  logout() {
    auth.logout('/login.html')
  }
}
