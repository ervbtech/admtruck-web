
export function configure(aurelia) {
    aurelia.use.standardConfiguration()
    aurelia.use.plugin('aurelia-dialog')
    aurelia.start().then(() => aurelia.setRoot('modules/nav-app'))
}
