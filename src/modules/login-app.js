import {AuthService}  from 'aurelia-auth'
import {inject}       from 'aurelia-framework'
import {MessageService} from 'services/message_service'
import {Gentelella}     from 'lib/gentelella'

let auth = {}, messageService = {}, gentelella = {}

@inject(AuthService, MessageService, Gentelella)
export class Login {

  constructor(AuthService, MessageService, Gentelella) {
    this.user = {}
    this.confirmationPassword = ''
    auth = AuthService
    messageService = MessageService
    gentelella = Gentelella
  }

  attached() {
    gentelella.init()

    $('input.submit').bind('keyup', (event) => {
      if(event.keyCode === 13) {
        this.login();
      }
    })
  }

  login() {
    auth.login(this.user.email, this.user.password)
      .catch(err => {
        messageService.showDark('Usuário não encontrado, verifique suas credenciais e tente novamente.')
      })
  }

  signup() {
    auth.signup(this.user.name, this.user.email, this.user.password)
      .then(response => {
        this.user = {}
        messageService.showSuccess('Cadastro realizado com sucesso. Aguarde a ativação de seu usuário para acessar o sistema.')
      })
      .catch((err) => {
        messageService.showError('Ops... ocorreu um erro durante a operação.')
      })
  }

  getCurrentDate() {
    return new Date()
  }

}
