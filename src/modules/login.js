import config from '../../authconfig'

export function configure(aurelia) {
    aurelia.use.standardConfiguration()
    aurelia.use.plugin('aurelia-auth', (baseConfig)=>{
         baseConfig.configure(config)
    })
    aurelia.start().then(() => aurelia.setRoot('modules/login-app'))
}
