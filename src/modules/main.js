export function configure(aurelia) {
  aurelia.use.standardConfiguration()
  aurelia.use.developmentLogging()
  aurelia.use.globalResources("aurelia-mask/masked-input")
  aurelia.use.plugin("aurelia-files")
  aurelia.use.plugin('aurelia-dialog')
  aurelia.use.plugin('aurelia-bootstrap-datepicker')
  aurelia.start().then(() => {
    aurelia.setRoot()
  })
}
