'use strict'

import {bindable, bindingMode, inject, customElement}  from 'aurelia-framework'
import {HttpClient, json}   from 'aurelia-fetch-client'
import {DialogService}      from 'aurelia-dialog'
import {AttachmentService}  from 'services/attachment_service'
import {CropperModal}       from 'components/custom/cropper/cropper-modal'
import {Enviroment}         from 'utils/enviroment'
import {AuthService}          from 'aurelia-auth'

@customElement('avatarview') // Define the name of our custom element
@inject(Element, HttpClient, DialogService, AttachmentService, Enviroment, AuthService) // Inject the instance of this element
export class AvatarView {

  @bindable clickable = false

  constructor(Element, HttpClient, DialogService, AttachmentService, Enviroment, AuthService) {
    this.element = Element
    this.env = Enviroment
    this.http = HttpClient
    this.user = AuthService.getTokenPayload().data
    this.services = []
    this.services['attachment_service'] = AttachmentService
    this.services['dialog_service'] = DialogService
  }

  attached() {
    let src = this.getUriImage(this.user._id)

    let parent = $(this.element).find('img').parent()

    if(parent.is('[class]')) {
      $(this.element).find('img').addClass(parent.attr('class'))
      parent.attr('class', '')
    }

    this.http.fetch(src)
    .then(response => {
      this.src = response.ok ? response.url : 'src/images/user.png'
    })
  }

  getUriImage(subpath) {
    return this.env.getUri('public/images/' + subpath)
  }

  showCropperModal() {
    if(!this.clickable)
      return;
    this.services['dialog_service'].open({viewModel: CropperModal, model: { image: this.src }}).then(response => {
      if (!response.wasCancelled) {
        var blobBin = atob(response.output.split(',')[1]);
        var array = [];
        for(var i = 0; i < blobBin.length; i++) {
          array.push(blobBin.charCodeAt(i));
        }

        let photo = new Blob([new Uint8Array(array)], {type: 'image/png'});

        this.services['attachment_service'].upload([{ file: photo, name: this.user._id }])
        .then(data => {
          // BROADCAST FOR UPDATE AOO INSTANCES FOR THIS COMPONENT
          $('avatarview').find('img').attr('src', response.output)
        })
      }
    });
  }


}
