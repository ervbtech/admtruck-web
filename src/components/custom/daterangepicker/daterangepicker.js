'use strict'


// Aurelia Framework specific functionality
import {bindable, inject, customElement} from 'aurelia-framework';

import rangepicker from 'node_modules/gentelella/production/js/datepicker/daterangepicker'
import moment      from 'moment'
import 'moment/locale/pt-br'

const formatText = 'MMMM [de] YYYY'

@customElement('daterangepicker')
@inject(Element)
export class DateRangePicker {

  constructor(element) {
    this.element = element
    this.formatDateFrom = moment().format(formatText)
    this.formatDateTo = moment().format(formatText)
  }

  attached() {
    let $this = this
    var cb = function(start, end, label) {
      $this.formatDateFrom = moment().format(formatText)
      $this.formatDateTo = moment().format(formatText)
    };

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      dateLimit: {
        days: 60
      },
      singleDatePicker: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      opens: 'left',
        buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      format: 'MM/DD/YYYY',
      separator: ' até ',
      locale: {
        applyLabel: 'Ok!',
        cancelLabel: 'Limpar',
        fromLabel: 'De',
        toLabel: 'Até',
        customRangeLabel: 'Custom',
        daysOfWeek: moment.weekdaysShort(),
        monthNames: moment.months(),
        firstDay: 1
      }
    };
    $('.reportrange').daterangepicker(optionSet1, cb);
    $('.reportrange').on('show.daterangepicker', function() {

    });
    $('.reportrange').on('hide.daterangepicker', function() {

    });
    $('.reportrange').on('apply.daterangepicker', function(ev, picker) {
        $this.formatDateFrom = picker.startDate.format(formatText)
        $this.formatDateTo = picker.endDate.format(formatText)

        let changeEvent;
        if (window.CustomEvent) {
          changeEvent = new CustomEvent('change', {
            detail: {
                startDate: picker.startDate
              , endDate: picker.endDate
            },
            bubbles: true
          });
        } else {
          changeEvent = document.createEvent('CustomEvent');
          changeEvent.initCustomEvent('change', true, true, {
            detail: {
                startDate: picker.startDate
              , endDate: picker.endDate
            }
          });
        }
        $this.element.dispatchEvent(changeEvent);
    });
    $('.reportrange').on('cancel.daterangepicker', function(ev, picker) {

    });
    $('#options1').click(function() {
      $('.reportrange').data('daterangepicker').setOptions(optionSet1, cb);
    });
    $('#options2').click(function() {
      $('.reportrange').data('daterangepicker').setOptions(optionSet2, cb);
    });
    $('#destroy').click(function() {
      $('.reportrange').data('daterangepicker').remove();
    });
  }

}
