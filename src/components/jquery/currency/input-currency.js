// Aurelia Framework specific functionality
import {bindable, bindingMode, inject, customElement} from 'aurelia-framework';

/* JQUERY PLUGINS */
import maskmoney from 'plentz/jquery-maskmoney/dist/jquery.maskMoney.min'

@customElement('inputcurrency') // Define the name of our custom element
@inject(Element) // Inject the instance of this element
export class InputCurrency {

  @bindable({ defaultBindingMode: bindingMode.twoWay }) value = null; // The default selected value

  constructor(element) {
    this.element = element
  }

  attached() {
    let input = $(this.element).find('input:first')

    if($(this.element).is('[required]')) {
      input.attr('required', 'required')
    } else {
      input.removeAttr('required')
    }

    input.maskMoney({
      thousands: '.'
      , decimal: ','
    })
    .on('change', (event) => {
      let changeEvent;
      if (window.CustomEvent) {
        changeEvent = new CustomEvent('change', {
          detail: {
            value: $(event.target).maskMoney('unmasked')[0]
          },
          bubbles: true
        });
      } else {
        changeEvent = document.createEvent('CustomEvent');
        changeEvent.initCustomEvent('change', true, true, {
          detail: {
            value: $(event.target).maskMoney('unmasked')[0]
          }
        });
      }
      this.element.dispatchEvent(changeEvent);
    })

  }

}
