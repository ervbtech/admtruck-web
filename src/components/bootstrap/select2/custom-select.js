// Aurelia Framework specific functionality
import {bindable, bindingMode, inject, customElement} from 'aurelia-framework';

@customElement('select2') // Define the name of our custom element
@inject(Element) // Inject the instance of this element
export class CustomSelect {
  @bindable name = null; // The name of our custom select
  @bindable({ defaultBindingMode: bindingMode.twoWay }) selected = null; // The default selected value
  @bindable options = {}; // The label/option values
  @bindable placeholder = null;

  constructor(element) {
    this.element = element;
  }

  // Once the Custom Element has its DOM instantiated and ready for binding
  // to happenings within the DOM itself
  attached() {
    $(this.element).find('select').prop('required', $(this.element).context.attributes['required'])
    $(this.element).find('select')
    .select2({
      placeholder: $(this).attr('placeholder'),
      allowClear: true
    })
    .on('change', (event) => {
      let changeEvent;
      if (window.CustomEvent) {
        changeEvent = new CustomEvent('change', {
          detail: {
            value: event.target.value
          },
          bubbles: true
        });
      } else {
        changeEvent = document.createEvent('CustomEvent');
        changeEvent.initCustomEvent('change', true, true, {
          detail: {
            value: event.target.value
          }
        });
      }
      this.element.dispatchEvent(changeEvent);
    });
  }
}
