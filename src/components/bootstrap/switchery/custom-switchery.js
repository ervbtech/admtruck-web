import {bindable, bindingMode, inject, customElement} from 'aurelia-framework'
import Switchery    from 'node_modules/gentelella/vendors/switchery/dist/switchery.min'

import 'node_modules/gentelella/vendors/switchery/dist/switchery.min.css!'

@customElement('switchery')
@inject(Element)
export class CustomSwitchery {

  @bindable({ defaultBindingMode: bindingMode.twoWay }) checked = null

  constructor(element) {
    this.element = element
  }

  attached() {
    setTimeout(() => {
      var switchery = new Switchery($(this.element).find('.js-switch').get(0), {
        color: '#26B99A'
      })
    }, 30)
  }
}
