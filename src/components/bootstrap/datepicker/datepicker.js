'use strict'

// Aurelia Framework specific functionality
import {bindable, inject, customElement} from 'aurelia-framework';

import 'bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR'

@customElement('datepicker') // Define the name of our custom element
@inject(Element) // Inject the instance of this element
export class Datepicker {

  @bindable name = null
  @bindable value
  @bindable placeholder = null
  @bindable dpOptions = {
    language: 'pt-BR',
    autoclose: true,
    templates: {
          leftArrow: '<i class="fa fa-arrow-left"></i>',
          rightArrow: '<i class="fa fa-arrow-right"></i>',
    }
  };

  constructor(element) {
    this.element = element
  }

  attached() {
    var attrs = $(this.element).context.attributes
    $(this.element).find('input').datepicker({
        language: "pt-BR",
        autoclose: true,
        templates: {
              leftArrow: '<i class="fa fa-arrow-left"></i>',
              rightArrow: '<i class="fa fa-arrow-right"></i>',
        }
    }).on('changeDate', (event) => {
      let changeEvent;
      if (window.CustomEvent) {
        changeEvent = new CustomEvent('change', {
          detail: {
            value: event.date
          },
          bubbles: true
        });
      } else {
        changeEvent = document.createEvent('CustomEvent');
        changeEvent.initCustomEvent('change', true, true, {
          detail: {
            value: event.date
          }
        });
      }
      this.element.dispatchEvent(changeEvent);
    })

    $(this.element).find('input').prop('required', attrs['required'])
    $(this.element).find('input').removeClass('datepicker')
  }

}
