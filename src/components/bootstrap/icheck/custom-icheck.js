// Aurelia Framework specific functionality
import {bindable, inject, customElement} from 'aurelia-framework';

@customElement('icheck') // Define the name of our custom element
@inject(Element) // Inject the instance of this element
export class CustomIcheck {

  @bindable type
  @bindable name
  @bindable value

  constructor(element) {
    this.element = element
  }

  attached() {
    let $this = this
    $(this.element).find('input:first').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    })
    .on('ifChanged', (event) => {
      let changeEvent;
      if (window.CustomEvent) {
        changeEvent = new CustomEvent('change', {
          detail: {
            value: event.target ? event.target.value : null
          },
          bubbles: true
        });
      } else {
        changeEvent = document.createEvent('CustomEvent');
        changeEvent.initCustomEvent('change', true, true, {
          detail: {
            value: event.target ? event.target.value : null
          }
        });
      }
      this.element.dispatchEvent(changeEvent);
    })
  }

}
