/** IMPORT GENTELELLA SCRIPTS DEPENDENCIES */
import $            from 'jquery'
import bootstrap    from 'bootstrap'
import fastclick    from 'node_modules/gentelella/vendors/fastclick/lib/fastclick'
import nprogress    from 'node_modules/gentelella/vendors/nprogress/nprogress'
import bsprogress   from 'node_modules/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min'
import smartWizard  from 'node_modules/gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard'
import parsley      from 'node_modules/gentelella/vendors/parsleyjs/dist/parsley.min'
import pslyi18nPtBR from 'node_modules/gentelella/vendors/parsleyjs/dist/i18n/pt-br'
import iCheck       from 'node_modules/gentelella/vendors/iCheck/icheck.min'
import select2      from 'node_modules/gentelella/vendors/select2/dist/js/select2.full.min'
import knob         from 'node_modules/gentelella/vendors/jquery-knob/dist/jquery.knob.min'
import cropper      from 'node_modules/gentelella/vendors/cropper/dist/cropper.min'
import rangeSlider  from 'node_modules/gentelella/vendors/ion.rangeSlider/js/ion.rangeSlider.min'

import a  from 'node_modules/gentelella/vendors/datatables.net/js/jquery.dataTables.min'

/* STYLES */
import 'node_modules/gentelella/vendors/pnotify/dist/pnotify.css!'
import 'node_modules/gentelella/vendors/pnotify/dist/pnotify.buttons.css!'
import 'node_modules/gentelella/vendors/iCheck/skins/flat/green.css!'

import 'node_modules/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css!'
import 'node_modules/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css!'
import 'node_modules/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css!'
import 'node_modules/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css!'
import 'node_modules/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css!'
import 'node_modules/gentelella/vendors/cropper/dist/cropper.min.css!'
import 'node_modules/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.css!'
import 'node_modules/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css!'

export function gentelella() {

}

export class Gentelella {

  init() {
    var CURRENT_URL = window.location.href.split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

    // Sidebar
    $(document).ready(function() {

      // TODO: This is some kind of easy fix, maybe we can improve this
      var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
        footerHeight = $BODY.hasClass('footer_fixed') ? 0 : $FOOTER.height(),
        leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
        contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
      };
      $SIDEBAR_MENU.find('a').unbind('click');
      $SIDEBAR_MENU.find('a').on('click', function(ev) {
        var $li = $(this).parent();

        if($li.find('li').length == 0) {
          $li.parent().find('li').removeClass('active active-sm');
          $li.parent().find('li').removeClass('current-page');
        }

        if ($li.is('.active')) {
          $li.removeClass('active active-sm');
          $('ul:first', $li).slideUp(function() {
            setContentHeight();
          });
        } else {
          // prevent closing menu if we are on child menu
          if (!$li.parent().is('.child_menu')) {
            $SIDEBAR_MENU.find('li').removeClass('active active-sm');
            $SIDEBAR_MENU.find('li').removeClass('current-page');
            $SIDEBAR_MENU.find('li ul').slideUp();
          }

          $li.addClass('active');

          $('ul:first', $li).slideDown(function() {
            setContentHeight();
          });
        }

      });

      // toggle small or large menu
      $MENU_TOGGLE.unbind('click');
      $MENU_TOGGLE.on('click', function() {
        if ($BODY.hasClass('nav-md')) {
          $SIDEBAR_MENU.find('li.active ul').hide();
          $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        } else {
          $SIDEBAR_MENU.find('li.active-sm ul').show();
          $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $BODY.toggleClass('nav-md nav-sm');

        setContentHeight();
      });

      // check active menu
      $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');


      $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
      }).parent('li').addClass('active current-page').parents('ul').slideDown(function() {
        setContentHeight();
      }).parent().addClass('active');

      // setContentHeight();

      // fixed sidebar
      if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
          autoHideScrollbar: true,
          theme: 'minimal',
          mouseWheel:{ preventDefault: true }
        });
      }
    });
    // /Sidebar

    // Panel toolbox
    $(document).ready(function() {
      $('.collapse-link').unbind('click')
      $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
        $ICON = $(this).find('i'),
        $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
          $BOX_CONTENT.slideToggle(200, function(){
            $BOX_PANEL.removeAttr('style');
          });
        } else {
          $BOX_CONTENT.slideToggle(200);
          $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
      });
      $('.close-link').unbind('click')
      $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
      });
    });
    // /Panel toolbox

    // Tooltip
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
      });
    });
    // /Tooltip

    // Progressbar
    if ($(".progress .progress-bar")[0]) {
      $('.progress .progress-bar').progressbar();
    }
    // /Progressbar

    // Table
    $('table input').on('ifChecked', function () {
      checkState = '';
      $(this).parent().parent().parent().addClass('selected');
      countChecked();
    });
    $('table input').on('ifUnchecked', function () {
      checkState = '';
      $(this).parent().parent().parent().removeClass('selected');
      countChecked();
    });

    var checkState = '';

    $('.bulk_action input').on('ifChecked', function () {
      checkState = '';
      $(this).parent().parent().parent().addClass('selected');
      countChecked();
    });
    $('.bulk_action input').on('ifUnchecked', function () {
      checkState = '';
      $(this).parent().parent().parent().removeClass('selected');
      countChecked();
    });
    $('.bulk_action input#check-all').on('ifChecked', function () {
      checkState = 'all';
      countChecked();
    });
    $('.bulk_action input#check-all').on('ifUnchecked', function () {
      checkState = 'none';
      countChecked();
    });

    function countChecked() {
      if (checkState === 'all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
      }
      if (checkState === 'none') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
      }

      var checkCount = $(".bulk_action input[name='table_records']:checked").length;

      if (checkCount) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(checkCount + ' Records Selected');
      } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
      }
    }

    // Accordion
    $(document).ready(function() {
      $(".expand").unbind('click');
      $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
          $expand.text("-");
        } else {
          $expand.text("+");
        }
      });
    });

    // jQuery Knob
    $(".knob").knob({
      change: function(value) {
        //console.log("change : " + value);
      },
      release: function(value) {
        //console.log(this.$.attr('value'));
        console.log("release : " + value);
      },
      cancel: function() {
        console.log("cancel : ", this);
      },
      /*format : function (value) {
      return value + '%';
    },*/
    draw: function() {

      // "tron" case
      if (this.$.data('skin') == 'tron') {

        this.cursorExt = 0.3;

        var a = this.arc(this.cv) // Arc
        ,
        pa // Previous arc
        , r = 1;

        this.g.lineWidth = this.lineWidth;

        if (this.o.displayPrevious) {
          pa = this.arc(this.v);
          this.g.beginPath();
          this.g.strokeStyle = this.pColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
          this.g.stroke();
        }

        this.g.beginPath();
        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
        this.g.stroke();

        this.g.lineWidth = 2;
        this.g.beginPath();
        this.g.strokeStyle = this.o.fgColor;
        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
        this.g.stroke();

        return false;
      }
    }
  });

  // NProgress
  if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
      NProgress.start();
    });

    $(window).load(function () {
      NProgress.done();
    });
  }
  /**
  * Resize function without multiple trigger
  *
  * Usage:
  * $(window).smartresize(function(){
  *     // code here
  * });
  */
  (function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
        var obj = this, args = arguments;
        function delayed () {
          if (!execAsap)
          func.apply(obj, args);
          timeout = null;
        }

        if (timeout)
        clearTimeout(timeout);
        else if (execAsap)
        func.apply(obj, args);

        timeout = setTimeout(delayed, threshold || 100);
      };
    };

    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

  })(jQuery,'smartresize');
}

}
