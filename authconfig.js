const configForDevelopment = {
  //Don't redirect after signup
  loginOnSignup: false,
  // Node API URI
  baseUrl: 'http://localhost:3000/api',
  // Endpoint to new users
  signupUrl: '/users/signup',
  // Endpoint to user login
  loginUrl: '/users/signin',
  // The API serves its tokens with a key of id_token which differs from
  // aureliauth's standard.
  tokenName: 'token',
  // path to redirect after login
  loginRedirect: '/#/dashboard'

}

const configForProduction = {
  //Don't redirect after signup
  loginOnSignup: false,
  // Node API URI
  baseUrl: 'http://ec2-35-164-32-217.us-west-2.compute.amazonaws.com:8080/api',
  // Endpoint to new users
  signupUrl: '/users/signup',
  // Endpoint to user login
  loginUrl: '/users/signin',
  // The API serves its tokens with a key of id_token which differs from
  // aureliauth's standard.
  tokenName: 'token',
  // path to redirect after login
  loginRedirect: '/#/dashboard'

}

let config = {}
if (window.location.hostname ==='localhost') {
    config = configForDevelopment
}
else{
    config = configForProduction
}

export default config
